using System;
using System.Linq;
namespace ConsoleUI
{
    public class Menu
    {
        public void Screen(){
        Solution solution = new Solution();

            var exit = false;
            var option = "";
            const string ONE = "1";
            const string TWO = "2";
            const string EXIT = "E";

            while (!exit)
            {
                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine("SELECT AN OPTION : ");
                Console.WriteLine($"Two numbers such that they add up to target  [{ONE}]");
                Console.WriteLine($"Is palindrome integer                        [{TWO}]");
                Console.WriteLine($"Exit                                         [{EXIT}]");

                Console.Write("option = ");
                option = Console.ReadLine();

                switch (option)
                {
                    case ONE:
                        solution.One();
                    break;
                    case TWO:
                        solution.Two();
                    break;
                    case EXIT:
                        exit = true;
                        break;
                }
            }
        }
    }
}