using System;
using System.Linq;

namespace ConsoleUI
{
    public class Solution
    {
        public void One()
        {
            Console.WriteLine("Numbers separated by , : ");
            string numbers = Console.ReadLine();
            string[] array = numbers.Split(',');
            int[] arrayInt = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                arrayInt[i] = Convert.ToInt32(array[i]);
            }

            int[] list = new int[arrayInt.Length];
            for (int i = 0; i < arrayInt.Length; i++)
            {
                int count = 0;
                for (int j = 0; j < arrayInt.Length; j++)
                {
                    if (arrayInt[i] == arrayInt[j])
                    {
                        count++;
                        if (number(arrayInt[i]))
                        {
                            list[i] = arrayInt[i];
                        }
                    }
                }
                if ((list[i] != 0) && count == 1)
                {
                    Console.WriteLine(list[i]);
                }
            }
            bool number(int num)
            {
                for (int i = 0; i < list.Length; i++)
                {
                    if (list[i]==num)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        public void Two()
        {
            Console.WriteLine("First word :");
            char[] firstWord = Console.ReadLine().ToCharArray();
            Console.WriteLine("Second word :");
            char[] secondtWord = Console.ReadLine().ToCharArray();
            Console.WriteLine("Third word :");
            char[] thirdWord = Console.ReadLine().ToCharArray();
            string common="";
            int lenght= 0;
            int[] longitudes = { firstWord.Length, secondtWord.Length, thirdWord.Length };
            lenght = longitudes.Min();

            Console.WriteLine("longitud " + lenght);
            
            for (int i = 0; i < lenght; i++)
            {
                if ((firstWord[i] == secondtWord[i]) && (firstWord[i] == thirdWord[i]))
                {
                    common = common + firstWord[i];
                }
            }
            if (common == "")
            {
                Console.WriteLine("\"\"");
            }
            else
            {
                Console.WriteLine(common);
            }
        }
    }
}