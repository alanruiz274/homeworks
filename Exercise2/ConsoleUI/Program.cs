﻿using System;
namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Solution solution = new Solution();

            var exit = false;
            var option = "";
            const string ONE = "1";
            const string TWO = "2";
            const string EXIT = "E";

            while (!exit)
            {
                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine("SELECT AN OPTION : ");
                Console.WriteLine($"Every element appears twice except for one  [{ONE}]");
                Console.WriteLine($"the longest common prefix                   [{TWO}]");
                Console.WriteLine($"Exit                                        [{EXIT}]");

                Console.Write("option = ");
                option = Console.ReadLine();

                switch (option)
                {
                    case ONE:
                        solution.One();
                    break;
                    case TWO:
                        solution.Two();
                    break;
                    case EXIT:
                        exit = true;
                        break;
                }
            }
        }
    }
}
