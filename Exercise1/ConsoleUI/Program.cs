﻿using System;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            Exercises exercises = new Exercises();

            var exit = false;
            var option = "";
            const string ONE = "1";
            const string TWO = "2";
            const string THREE = "3";
            const string FOUR = "4";
            const string EXIT = "E";

            while (!exit)
            {
                Console.WriteLine("--------------------------------------------------------");
                Console.WriteLine("SELECT AN OPTION : ");
                Console.WriteLine($"Two numbers such that they add up to target  [{ONE}]");
                Console.WriteLine($"Is palindrome integer                        [{TWO}]");
                Console.WriteLine($"Determine if the input string is valid       [{THREE}]");
                Console.WriteLine($"Minimum and maximum numbers                  [{FOUR}]");
                Console.WriteLine($"Exit                                         [{EXIT}]");

                Console.Write("option = ");
                option = Console.ReadLine();

                switch (option)
                {
                    case ONE:
                        exercises.One();
                    break;
                    case TWO:
                        exercises.Two();
                    break;
                    case THREE:
                        exercises.Three();
                    break;
                    case FOUR:
                        exercises.Four();
                    break;
                    case EXIT:
                        exit = true;
                        break;
                }
            }

            Console.ReadKey();
        }
    }
}
