using System;
using System.Linq;

namespace ConsoleUI
{
    public class Exercises
    {
        public void One() 
        {
            //example1 
            string numeros;
            int target;
            int sum;
            Console.WriteLine("Numbers separated by , : ");
            numeros = Console.ReadLine();
            Console.WriteLine("Target: ");
            target = Convert.ToInt32(Console.ReadLine());

            string[] arreglo = numeros.Split(',');

            for (int n = 0; n < arreglo.Length; n++)
            {
                for (int n2 = 0; n2 < arreglo.Length; n2++)
                {
                    sum = 0;
                    sum = Convert.ToInt32(arreglo[n]) + Convert.ToInt32(arreglo[n2]);
                    if (sum == target && n != n2)
                    {
                        Console.WriteLine($"valor == [{n},{n2}]");
                        n = arreglo.Length + 1;
                    }
                }
            }
        }

        public void Two()
        {
            //palindrome
            string number, reverse;
            Console.WriteLine("Enter number : ");
            char[] array = Console.ReadLine().ToCharArray();
            number = new string(array);
            
            Array.Reverse(array);

            reverse = new string(array);
            //Console.WriteLine("Comparation : " + number + " \n Reverse : " + reverse);
            if (number == reverse)
            {
                Console.WriteLine($"Palindrome = {true}");
            }
            else 
            {
                Console.WriteLine($"Palindrome = {false}");
            }
        }
        public void Three() 
        {
            //brackets
            Console.WriteLine("Writte (){}[]");
            string cadena = Console.ReadLine();
            char[] array = cadena.ToCharArray();
            DoThree(array);
        }
        void DoThree(char[] array) 
        {
            int bracket = 0, bracket2 = 0, bracket3 = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == '(')
                    bracket += 1;
                if (array[i] == ')')
                    bracket -= 1;
                if (array[i] == '{')
                    bracket2 += 1;
                if (array[i] == '}')
                    bracket2 -= 1;
                if (array[i] == '[')
                    bracket3 += 1;
                if (array[i] == ']')
                    bracket3 -= 1;
                if ((i > 0) && (array[i] == ')' && (array[i - 1] == '{' || array[i - 1] == '[')))
                    bracket -= 5;
                if ((i > 0) && (array[i] == '}' && (array[i - 1] == '(' || array[i - 1] == '[')))
                    bracket2 -= 5;
                if ((i > 0) && (array[i] == ']' && (array[i - 1] == '{' || array[i - 1] == '(')))
                    bracket -= 5;
            }

            if (bracket != 0 || bracket2 != 0 || bracket3 != 0)
            {
                Console.WriteLine(false);
            }
            else
            {
                Console.WriteLine(true);
            }
        }
        public void Four()
        {
            //array min and max
            Console.WriteLine("Numbers separated by , : ");
            string numeros = Console.ReadLine();
            
            string[] array = numeros.Split(',');

            int[] arrayInt = new int[array.Length];
            for (int i = 0; i < array.Length; i++)
            {
                arrayInt[i] = Convert.ToInt32(array[i]);
            }

            Console.WriteLine($"[{arrayInt.Min()},{arrayInt.Max()}]");
        }
    }
}