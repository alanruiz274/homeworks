using System;
namespace Practice5
{
    public class Screen
    {
        private readonly Alphabet _alphabet;
        private readonly Intersection _intersection;
        public Screen(Alphabet alphabet, Intersection intersection)
        {
            _alphabet = alphabet;
            _intersection = intersection;
        }
        public void Show()
        {
            var exit = false;
            var option = "";
            const string FIRST = "1";
            const string SECOND = "2";
            const string EXIT = "E";


            while (!exit)
            {
                Console.WriteLine("------------------------------------------------------------------");
                Console.WriteLine("Practice 5");
                Console.WriteLine("------------------------------------------------------------------");
                Console.WriteLine("Options");
                Console.WriteLine($"Check if is a pangram       [{FIRST}]");
                Console.WriteLine($"Intersection                [{SECOND}]");
                Console.WriteLine($"Exit                        [{EXIT}]");
                
                Console.WriteLine("Select an option : ");
                option = Console.ReadLine();

                switch (option)
                {
                    case FIRST:
                        _alphabet.Pangram();
                        break;
                        ;
                    case SECOND:
                        _intersection.Intersections();
                        break;
                    case EXIT:
                        exit = true;
                        break;
                }
            }
        }
    }
}