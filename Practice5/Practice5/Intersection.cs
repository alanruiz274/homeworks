using System;
using System.Linq;

namespace Practice5
{
    public class Intersection
    {
        public void Intersections()
        {
            Console.WriteLine("numbers must be separated by ,\nfirst numbers :");
            string[] firstNumbers = Console.ReadLine().Split(',');
            Console.WriteLine("second numbers :");
            string[] secondNumbers = Console.ReadLine().Split(',');
            string output = "";
            Compare(firstNumbers, secondNumbers, ref output);
            Console.WriteLine($"[{output}]");
        }
        private void Compare(string[] firstNumbers, string[] secondNumbers, ref string output)
        {
            for (int i = 0; i < firstNumbers.Length; i++)
            {
                for (int j = 0; j < secondNumbers.Length; j++)
                {
                    if (firstNumbers[i] == secondNumbers[j] && firstNumbers[i] != "")
                    {
                        output = output + "," + firstNumbers[i];
                        firstNumbers[i] = "";
                        secondNumbers[j] = "";
                    }
                }
            }
        }
    }
}