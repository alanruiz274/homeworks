﻿using System;

namespace Practice5
{
    class Program
    {
        static void Main(string[] args)
        {
            var alphabet = new Alphabet();
            var intersection = new Intersection();

            var screen = new Screen(alphabet,intersection);
            
            screen.Show();
            Console.ReadLine();
        }
    }
}
