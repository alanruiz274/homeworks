using System;
using System.Linq;

namespace Practice5
{
    public class Alphabet
    {
        public void Pangram()
        {
            char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g','h', 'i', 'j', 'k', 'l', 'm','n','o','p','q','r','s','t','u','v','w','x','y','z'};
            int[] repeats = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            Console.WriteLine("Give a string : ");
            char[] array = Console.ReadLine().ToCharArray();
            Compare(alphabet,array,repeats);
            IsOrNot(repeats);
        }
        private void Compare(char[] alphabet,char[] array,int[] repeats)
        {
            for (int i = 0; i < alphabet.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    if (alphabet[i] == array[j])
                    {
                        repeats[i] += 1;
                    }
                }
                //Console.WriteLine($"L : {alphabet[i]} repeats : {repeats[i]}");
            }
        }
        private void IsOrNot(int[] repeats)
        {
            if (repeats.Min() == 0)
            {
                Console.WriteLine("Is not a Pangram");
            }
            else
            {
                Console.WriteLine("Is a Pangram");
            }
        }
    }
}