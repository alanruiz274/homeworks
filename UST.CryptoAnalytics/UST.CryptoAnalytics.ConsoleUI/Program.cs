﻿using System;

using UST.CryptoAnalytics.ConsoleUI.Screens;

using UST.CryptoAnalytics.Services;
using UST.CryptoAnalytics.Repository;
using UST.CryptoAnalytics.Repository.Interfaces;
using UST.CryptoAnalytics.Services.Interfaces;

namespace UST.CryptoAnalytics.ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            ICryptoRepository cryptoRepository = new CryptoRepository();
            ICryptoService cryptoService = new CryptoService(cryptoRepository);

            var bookScreen = new BooksScreen(cryptoService);
            
            var currrenciesInfoScreen = new CurrrenciesInfoScreen();
            var splashScreen = new SplashScreencs();
            var tickerBookScreen = new TickerBookScreen(cryptoService);

            var mainscreeen = new MainScreen(bookScreen,currrenciesInfoScreen,splashScreen,tickerBookScreen);
            
            mainscreeen.Show();
            Console.ReadLine();
        }
    }
}
