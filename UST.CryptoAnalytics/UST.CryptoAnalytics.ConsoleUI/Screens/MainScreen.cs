using System;
using UST.CryptoAnalytics.Services;

namespace UST.CryptoAnalytics.ConsoleUI.Screens
{
    public class MainScreen : BaseScreen
    {
        private readonly BooksScreen _bookScreeen;
        private readonly CurrrenciesInfoScreen _currrenciesInfoScreen;
        private readonly SplashScreencs _splashScreencs;
        private readonly TickerBookScreen _tickerBookScreen;

        public MainScreen (BooksScreen booksScreen,
        CurrrenciesInfoScreen currrenciesInfoScreen,
        SplashScreencs splashScreencs,
        TickerBookScreen tickerBookScreen)
        {
            _bookScreeen = booksScreen;
            _currrenciesInfoScreen = currrenciesInfoScreen;
            _splashScreencs = splashScreencs;
            _tickerBookScreen = tickerBookScreen;
        }
        private readonly CryptoService _cryptoService;

        public MainScreen(CryptoService cryptoService)
        {
            _cryptoService = cryptoService;
        }

        public override void Show()
        {
            var exit = false;
            var option = "";
            const string MENU_INFO_CRYPTO = "A";
            const string MENU_BOOKS = "B";
            const string MENU_TICKET = "C";
            const string MENU_EXIT = "D";


            while (!exit)
            {
                Console.WriteLine("UST CryptoAnalytics System");

                AddNewEmptyLine(2);

                Console.WriteLine("Select an option : ");
                Console.WriteLine($"Display available book      [{MENU_INFO_CRYPTO}]");
                Console.WriteLine($"Display available books     [{MENU_BOOKS}]");
                Console.WriteLine($"Display tickets books       [{MENU_TICKET}]");
                Console.WriteLine($"Exit                        [{MENU_EXIT}]");

                option = Console.ReadLine();

                switch (option)
                {
                    case MENU_INFO_CRYPTO:
                        _currrenciesInfoScreen.Show();
                        break;
                        ;
                    case MENU_BOOKS:
                        _bookScreeen.Show();
                        break;
                    case MENU_TICKET:
                        _tickerBookScreen.Show();
                        break;
                    case MENU_EXIT:
                        exit = true;
                        break;
                }

                Console.WriteLine(option);
            }

            Console.ReadLine();
        }

        /*private void DisplayCryptoCurrenciesInformation()
        {
            Console.Clear();
            Console.WriteLine("Information about Crypto currencies");
            AddNewEmptyLine(2);
            Console.WriteLine("Select the currency");

            var currencies = _cryptoService.GetCryptoCurrencies();

            foreach(var currency in currencies)
            {
                Console.WriteLine($"{currency.Name} [{currency.Code}]");
            }
        }*/

        
    }
}