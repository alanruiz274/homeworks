using System;

using UST.CryptoAnalytics.Entities;
using UST.CryptoAnalytics.Services;

using UST.CryptoAnalytics.Services.Interfaces;

namespace UST.CryptoAnalytics.ConsoleUI.Screens
{
    public class BooksScreen: BaseScreen
    {
        private readonly ICryptoService _cryptoService;
        
        public BooksScreen(ICryptoService cryptoService)
        {
            _cryptoService = cryptoService;
        }
        public override void Show()
        {
            Console.WriteLine("Available Books");
            
            var response = _cryptoService.GetBooks();
            if(response.Success)
            {
                
                foreach (var book in response.Payload)
                {
                    Console.WriteLine($"{book.Book}");
                }
            }
            /*Console.WriteLine("Showing BookScreen");
            var currencies = _cryptoService.GetCryptoCurrencies();
            foreach(var currency in currencies)
            {
                Console.WriteLine($"{currency.Code}");
            }*/
        }
    }
}