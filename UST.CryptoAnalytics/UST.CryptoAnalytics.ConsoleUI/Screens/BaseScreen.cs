using System;
namespace UST.CryptoAnalytics.ConsoleUI.Screens
{
    public abstract class BaseScreen
    {
        //override cuando es virtual o abstra
        public abstract void Show();
        protected void AddNewEmptyLine(int lines = 1)
        {
            for(int line = 0; line < lines; line++)
            {
                Console.WriteLine();
            }
        }
    }
}