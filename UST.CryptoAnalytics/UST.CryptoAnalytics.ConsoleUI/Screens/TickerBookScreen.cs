using System;
using UST.CryptoAnalytics.Entities;

using UST.CryptoAnalytics.Services.Interfaces;

namespace UST.CryptoAnalytics.ConsoleUI.Screens
{
    public class TickerBookScreen: BaseScreen
    {
        private readonly ICryptoService _cryptoService;
        public TickerBookScreen(ICryptoService cryptoService)
        {
            _cryptoService = cryptoService;
        }
        public override void Show()
        {
            Console.WriteLine("Select a ticker: ");
            string ticker = Console.ReadLine();
            var response = _cryptoService.GetTicker(ticker);
            if(response.Success)
            {
                var book = response.Payload;
                Console.WriteLine($"Book: {book.Book}\nVolume: {book.Volume}\nHigh: {book.High}\nLast: {book.Last}\nLow:{book.Low}\nVwap: {book.Vwap}\nAsk: {book.Ask}\nBid: {book.Bid}\nCreated at: {book.Created_at}");
                
            }
        }
    }
}