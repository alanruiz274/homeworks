using System.Collections.Generic;

using UST.CryptoAnalytics.Entities;
using UST.CryptoAnalytics.Api.Service.Interfaces;
using UST.CryptoAnalytics.Api.Repository.Interfaces;


namespace UST.CryptoAnalytics.Api.Service
{
    public class UserService: IUserService
    {
        private readonly IUserRepository _repository;
        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }
        public List<User> GetAll()
        {
            var users = _repository.GetAll();
            return users;
        }
        public User GetById(int id)
        {
            var user = _repository.GetById(id);
            
            return user;

        }
        public User Save(User user)
        {
            _repository.Save(user);
            return user;
        }
        public User UpdateById(User user)
        {
            _repository.UpdateById(user);
            return user;
        }
        public User DropById(int id)
        {
            var user = _repository.DropById(id);
            
            return user;
        }

    }
}