using System.Collections.Generic;
using System.Linq;
using UST.CryptoAnalytics.Entities;
using UST.CryptoAnalytics.Api.Repository.Interfaces;

namespace UST.CryptoAnalytics.Api.Repository
{
    public class MemoryUserRepository: IUserRepository
    {
        private List<User> _users;
        public MemoryUserRepository()
        {
            _users = new List<User>();
        }
        public List<User> GetAll()
        {
            return _users;
        }
        public User GetById(int id)
        {
            var user = _users.FirstOrDefault(u => u.Id ==id);
            return user;
        }
        public User Save(User user)
        {
            var id = _users.Count + 1;
            //_users.Id = id;
            _users.Add(user);
            return user;
        }
        public User UpdateById(User user)
        {
            _users.Add(user);
            return user;
        }
        public User DropById(int id)
        {
            var user = _users.FirstOrDefault(u => u.Id ==id);
            return user;
        }
    }
}