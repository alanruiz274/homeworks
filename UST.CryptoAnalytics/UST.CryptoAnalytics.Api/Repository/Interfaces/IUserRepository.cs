using System.Collections.Generic;
using UST.CryptoAnalytics.Entities;

namespace UST.CryptoAnalytics.Api.Repository.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetAll();
        User GetById(int id);
        User Save(User save);
        User UpdateById(User update);
        User DropById(int id);
    }
}