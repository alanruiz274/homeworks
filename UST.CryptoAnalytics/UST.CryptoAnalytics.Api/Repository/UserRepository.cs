using System.Collections.Generic;
using System.Linq;
using UST.CryptoAnalytics.Entities;
using UST.CryptoAnalytics.Api.Repository.Interfaces;

using System.Data.Common;
using MySqlConnector;
using Dapper;
using Dapper.Contrib.Extensions;


namespace UST.CryptoAnalytics.Api.Repository
{
    public class UserRepository: IUserRepository
    {
        private readonly MySqlConnection _connection; 
        public UserRepository()
        {
            _connection = new MySqlConnection("server=localhost;user=root;pwd=;database=UST;port=3306");
        }       
        //ado.net
        public List<User> GetAll()
        {
            var sql = "SELECT * FROM User";
            _connection.Open();
            var users = _connection.Query<User>(sql).ToList();
            _connection.Close();
            
            return users;
        }
        public User GetById(int id)
        {
            //var users = GetAll();
            //var user = users.FirstOrDefault(u => u.Id == id);
            var sql = "SELECT * FROM User WHERE Id = @Id";
            var users = _connection.Query<User>(sql, new{Id = id}).ToList();
            User user = (users != null && users.Count >0)? users[0] : null;
            return user;
        }
        public User Save(User user)
        {
            _connection.Insert<User>(user);

            return user;
        }
        public User UpdateById(User user)
        {
            _connection.Update<User>(user);
            return user;
        }
        public User DropById(int id)
        {
            var sql = "DELETE FROM User WHERE Id = @Id";
            var users = _connection.Query<User>(sql, new{Id = id}).ToList();
            User user = (users != null && users.Count >0)? users[0] : null;
            return user;
        }
    }
}