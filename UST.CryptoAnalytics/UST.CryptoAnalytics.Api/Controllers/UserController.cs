using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using UST.CryptoAnalytics.Api.Service;
using UST.CryptoAnalytics.Api.Service.Interfaces;

using UST.CryptoAnalytics.Entities;

namespace UST.CryptoAnalytics.Api.Controllers
{
    
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        [Route("api/[controller]")]
        public ActionResult<List<User>> GetAll()
        {
            var users = _userService.GetAll();

            //Ok(users);
            
            return Ok(users);
        }
        [HttpGet]
        [Route("api/[controller]/{id}")]
        public ActionResult<User> GetById(int id)
        {
            var user = _userService.GetById(id);

            //return NotFound();

            return Ok(user);
        }
        [HttpPost]
        [Route("api/[controller]")]
        public ActionResult<List<User>> Save([FromBody]User user)
        {
            _userService.Save(user);
            
            return Ok(user);
        }
        [HttpDelete]
        [Route("api/[controller]/{id}")]
        public ActionResult<User> DropById(int id)
        {
            var user = _userService.DropById(id);
            return Ok(user);
        }
        [HttpPut]
        [Route("api/[controller]")]
        public ActionResult<List<User>> UpdateById([FromBody]User user)
        {
            _userService.UpdateById(user);
            
            return Ok(user);
        }
    }
}