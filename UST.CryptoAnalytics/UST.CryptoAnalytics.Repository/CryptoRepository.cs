using System;
using System.Net.Http;
using System.Collections.Generic;

using Newtonsoft.Json;

using UST.CryptoAnalytics.Repository.Interfaces;
using UST.CryptoAnalytics.Entities;

namespace UST.CryptoAnalytics.Repository
{
    public class CryptoRepository : ICryptoRepository
    {
        private readonly HttpClient _httpClient;
        public CryptoRepository()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri("https://api.bitso.com/v3/");
        }
        public Response<List<CryptoBook>> GetBooks()
        {
            var books = new List<CryptoBook>();
            var response = new Response<List<CryptoBook>>();
            response.Payload = books;
            response.Success = true;
            //url
            //endpoint
            //client -> request
            //process to response
            var result = _httpClient.GetAsync("available_books").Result;

            //Console.WriteLine($"{result.IsSuccessStatusCode} {result.Content}");

            if (result.IsSuccessStatusCode)
            {
                var json = result.Content.ReadAsStringAsync().Result;
                response = JsonConvert.DeserializeObject<Response<List<CryptoBook>>>(json);
            }
            return response;
        }

        public Response<List<Ticker>> GetTickets()
        {
            var tickers = new List<Ticker>();
            var response = new Response<List<Ticker>>();

            response.Payload = tickers;
            response.Success = true;

            var result = _httpClient.GetAsync("ticker").Result;

            //Console.WriteLine($"{result.IsSuccessStatusCode} {result.Content}");

            if (result.IsSuccessStatusCode)
            {
                var json = result.Content.ReadAsStringAsync().Result;
                response = JsonConvert.DeserializeObject<Response<List<Ticker>>>(json);
            }

            return response;
        }
        public Response<Ticker> GetTicker(string book)
        {
            var ticker = new Ticker();
            var response = new Response<Ticker>();

            response.Payload = ticker;
            response.Success = true;
            
            var result = _httpClient.GetAsync($"ticker/?book={book}").Result;//btc_mxn
            //Console.WriteLine($"{result.IsSuccessStatusCode} {result.Content}");

            if (result.IsSuccessStatusCode)
            {
                var json = result.Content.ReadAsStringAsync().Result;
                response = JsonConvert.DeserializeObject<Response<Ticker>>(json);
            }
            return response;
        }
        public List<Crypto> GetCryptoCurrencies()
        {
            var cryptos = new List<Crypto>();

            cryptos.Add(new Crypto { Code = "BTC", Name = "Bitcoin" });
            cryptos.Add(new Crypto { Code = "FTH", Name = "Fthereum" });

            return cryptos;
        }
    }
}