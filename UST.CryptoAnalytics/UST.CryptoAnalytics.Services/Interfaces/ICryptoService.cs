using System;
using System.Collections.Generic;
using UST.CryptoAnalytics.Entities;

namespace UST.CryptoAnalytics.Services.Interfaces
{
    public interface ICryptoService
    {
        List<Crypto> GetCryptoCurrencies();
        Response<List<CryptoBook>> GetBooks();
        Response<List<Ticker>> GetTickets();
        Response<Ticker> GetTicker(string book);
    
    }
}
