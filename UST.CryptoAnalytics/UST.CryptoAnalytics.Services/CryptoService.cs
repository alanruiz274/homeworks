using System.Collections.Generic;

using UST.CryptoAnalytics.Entities;
using UST.CryptoAnalytics.Services.Interfaces;
using UST.CryptoAnalytics.Repository.Interfaces;

namespace UST.CryptoAnalytics.Services
{
    public class CryptoService: ICryptoService
    {
        private ICryptoRepository _cryptoRepository;
        public CryptoService(ICryptoRepository cryptoRepository)
        {
            _cryptoRepository = cryptoRepository;
        }
        //public List<Crypto> GetCryptos2 ()=> _crypto 
        public List<Crypto> GetCryptoCurrencies()
        {
            var currencies = _cryptoRepository.GetCryptoCurrencies();        
            return currencies;
        }
        public Response<List<CryptoBook>> GetBooks()
        {
            var books = _cryptoRepository.GetBooks();
            return books;
        }
        public Response<List<Ticker>> GetTickets()
        {
            var tickets = _cryptoRepository.GetTickets();
            return tickets;
        }
        public Response<Ticker> GetTicker(string book)
        {
            var ticket = _cryptoRepository.GetTicker(book);
            return ticket;
        }
    }
}