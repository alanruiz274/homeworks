namespace UST.CryptoAnalytics.Entities
{
    public class Ticker
    {
        public string Book{get; set;}
        public string Volume{get; set;}
        public string High{get; set;}
        public string Last{get; set;}
        public string Low{get; set;}
        public string Vwap{get; set;}
        public string Ask{get; set;}
        public string Bid{get; set;}
        public string Created_at{get; set;}
        
        
    }
}