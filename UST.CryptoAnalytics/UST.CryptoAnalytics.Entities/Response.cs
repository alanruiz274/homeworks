using System.Collections.Generic;

namespace UST.CryptoAnalytics.Entities
{
    public class Response<T>
    {
        public bool Success {get; set;}
        public T Payload{get; set;}
    }
}