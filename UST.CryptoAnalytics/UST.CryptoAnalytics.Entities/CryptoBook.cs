namespace UST.CryptoAnalytics.Entities
{
    public class CryptoBook
    {
        public string Book {get; set;}
        public string Minimum_amount {get; set;}
        public string Maximun_amount {get; set;}
        public string Minimum_price {get; set;}
        public string Maximun_price {get; set;}
        public string Minimum_value {get; set;}
        public string Maximun_value {get; set;}
    }
}